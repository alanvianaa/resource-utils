[![](https://jitpack.io/v/com.gitlab.calculato-oss/resource-utils.svg)](https://jitpack.io/#com.gitlab.calculato-oss/resource-utils)

![Squad calculato](https://img.shields.io/badge/squad-calculato-blue)
![dependency version](https://img.shields.io/badge/Kotlin_JVM-1.3.72-orange)
![dependency version](https://img.shields.io/badge/Ktor-1.3.2-orange)
![dependency version](https://img.shields.io/badge/Kmongo-4.0.2-orange)
![dependency version](https://img.shields.io/badge/Exposed-0.25.1-orange)
# Ktor Resource Utils

Biblioteca Kotlin com complementos para ajudar no uso do ktor em um sistema a nível de produção.

## O que essa biblioteca faz?

* Padronização de excessões
* Serialização e Deserialização para GSON de Classes do Java 8 (`LocalDateTime`, `LocalTime`, `LocalDate`, `ObjectId`)
* Serialização e Deserialização para GSON de Classe BSON (`ObjectId`)
* Modelo de dados de solicitação de páginas com base em cursor
* Função de encode e decode de String (base 64)

## Como usar

Passo 1: Adicione em repositories do arquivo build.gradle o jitpack.io:
```
repositories {
    maven { url 'https://jitpack.io' }
}
```
Passo 2: Adicione em dependencies
```
implementation "com.gitlab.calculato-oss:resource-utils:$version"
```

[Usar em outras ferramentas de build](https://jitpack.io/v/com.gitlab.calculato-oss/resource-utils)