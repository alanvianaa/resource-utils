package br.com.calculato.resourceutils

import io.ktor.http.HttpStatusCode
import kotlinx.serialization.Serializable
import org.apache.commons.lang3.StringUtils

/**
 * Object to be returned to HTTP request when an Exception is thrown
 */
@Serializable
class HttpException(val statusCode: Int, val developerMessage: String, val userMessage: String, val moreInfo: String) :
    RuntimeException()

/**
 * Creates a 4XX HTTP Exception to be thrown
 */
fun httpException(
    code: HttpStatusCode = HttpStatusCode.BadRequest,
    developerMessage: String = code.description,
    userMessage: String = "Desculpe, houve um erro na requisição, por favor tente novamente.",
    moreInfo: String = StringUtils.EMPTY
): Nothing = throw HttpException(code.value, developerMessage, userMessage, moreInfo)

/**
 * Creates a 5XX HTTP Exception to be thrown
 */
fun serverException(
    code: HttpStatusCode = HttpStatusCode.InternalServerError,
    developerMessage: String = code.description,
    userMessage: String = "Desculpe, houve um erro no processamento da requisição, por favor tente novamente.",
    moreInfo: String = StringUtils.EMPTY
): Nothing = throw HttpException(code.value, developerMessage, userMessage, moreInfo)

/**
 * Returned a Throwable exception
 */
@Serializable
class RequiredDependencyException(val msg: String) : Throwable(msg)
