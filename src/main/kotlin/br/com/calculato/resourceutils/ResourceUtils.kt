package br.com.calculato.resourceutils


import io.ktor.http.Parameters
import io.ktor.util.StringValues
import java.math.BigDecimal
import kotlinx.serialization.Serializable
import org.bson.types.ObjectId
import org.jetbrains.exposed.sql.SortOrder
import org.litote.kmongo.Id
import org.litote.kmongo.id.toId
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.UUID


/**
 * Default object to be used with GET requests with pagination
 */
data class CursorRequest(
    val cursor: String? = null,
    val size: Int = 15,
    val tags: List<String> = emptyList(),
    val orderProperty: CursorOrder = CursorOrder.NAME,
    val order: CursorOrderDirection = CursorOrderDirection.ASC,
    val search: String? = null
)

/**
 * Default object to be used with GET requests with pagination by ids
 */
data class IdsCursorRequest(
    val cursor: String? = null,
    val size: Int = 15,
    val tags: List<String> = emptyList(),
    val orderProperty: CursorOrder = CursorOrder.NAME,
    val order: CursorOrderDirection = CursorOrderDirection.ASC,
    val ids: List<UUID> = emptyList(),
    val startDate: LocalDate,
    val endDate: LocalDate
)

/**
 * Helper function to get the page request param out of the request
 */
fun Parameters.getIdsCursorRequest() = IdsCursorRequest(
    cursor = get("cursor"),
    size = get("size")?.toInt() ?: 15,
    tags = getAll("tags")?.filter { it.isNotBlank() } ?: emptyList(),
    orderProperty = get("orderProperty").let {
        if (it == null) CursorOrder.NAME else CursorOrder.valueOf(it)
    },
    order = get("orderDirection").let {
        if (it == null) CursorOrderDirection.ASC else CursorOrderDirection.valueOf(it)
    },
    ids = getAll("ids")?.map { UUID.fromString(it) } ?: emptyList(),
    startDate = get("startDate")?.let { LocalDate.parse(it) }
        ?: httpException(developerMessage = "Required parameter startDate is not present"),
    endDate = get("endDate")?.let { LocalDate.parse(it) }
        ?: httpException(developerMessage = "Required parameter endDate is not present")
)

/**
 * Enum helper for pagination property definition
 */
enum class CursorOrder { NAME, DATE, NUMBER, VALUE }

/**
 * Enum helper for pagination order definition
 */
enum class CursorOrderDirection { ASC, DESC }

/**
 * Converter to work with this enum order type to exposeds one.
 */
val CursorOrderDirection.exposedOrder: SortOrder
    get() = when (this) {
        CursorOrderDirection.ASC -> SortOrder.ASC
        CursorOrderDirection.DESC -> SortOrder.DESC
    }

/**
 * Helper function to get the page request param out of the request
 */
fun Parameters.getCursorRequest() = CursorRequest(
    cursor = get("cursor"),
    size = get("size")?.toInt() ?: 15,
    tags = getAll("tags")?.filter { it.isNotBlank() } ?: emptyList(),
    orderProperty = get("orderProperty").let {
        if (it == null) CursorOrder.NAME else CursorOrder.valueOf(it)
    },
    order = get("orderDirection").let {
        if (it == null) CursorOrderDirection.ASC else CursorOrderDirection.valueOf(it)
    },
    search = get("search")
)

/**
 * Helper to decode and and extract a [base64Cursor]
 */
@Suppress("IMPLICIT_CAST_TO_ANY")
fun extractCursor(base64Cursor: String, cursorOrder: CursorOrder): Pair<Any, UUID> {
    val splited = base64Cursor.decodeFromBase64().split("|", limit = 2)
    val second = UUID.fromString(splited.first())
    val first = splited.last()

    return ((if (cursorOrder == CursorOrder.DATE) LocalDateTime.parse(first) else first) to second)
}

fun createCursor(composition: Any, id: UUID): String = "$id|$composition".encodeToBase64()


/**
 * Cursor based page request data model containing a [data] list and a [cursor] from previous page
 */
@Serializable
data class CursorPage<T>(val data: List<T>, val cursor: String?, val total: Long)

/**
 * Cursor based page request data model containing a [data] list, a [totalValue] value and a [cursor] from previous page
 */
data class CursorPageValue<T>(
    val data: List<T>,
    val cursor: String?,
    val total: Long,
    @Serializable(BigDecimalSerializer::class) val totalValue: BigDecimal
)

/**
 * Class used to define pagination params for internal services usage
 * Returns a 64 base [cursor] and the [data] load
 */
@Serializable
data class InternalCursorPage<T>(val data: List<T>, val cursor: String?)

/**
 * Helper to decode and and extract a [base64Cursor] with localdate param
 */
fun extractDateCursor(base64Cursor: String): Pair<LocalDate, UUID> {
    val splited = base64Cursor.decodeFromBase64().split("|", limit = 2)
    val second = UUID.fromString(splited.first())
    val first = splited.last()

    return LocalDate.parse(first) to second
}

/**
 * Helper to decode and and extract a [base64Cursor] with localdatetime param
 */
fun extractDateTimeCursor(base64Cursor: String): Pair<LocalDateTime, UUID> {
    val splited = base64Cursor.decodeFromBase64().split("|", limit = 2)
    val second = UUID.fromString(splited.first())
    val first = splited.last()

    return LocalDateTime.parse(first) to second
}

/**
 * Helper to decode and and extract a [Int] of [this] cursorsize
 */
private fun String.extractCursorSize(): Int = decodeFromBase64().split("|", limit = 2).last().toInt()

inline fun <reified T> StringValues.receiveDocumentIdParam(name: String = "id"): Id<T> =
    getAll(name)?.firstOrNull()?.let {
        ObjectId(it).toId()
    } ?: httpException(developerMessage = "Parameter $name does not contain a valid Document ID value")

