package br.com.calculato.resourceutils

/**
 * Validation helper for types serialization
 */
fun validate(value: Boolean, userMessage: () -> String) {
    if (!value) httpException(
        developerMessage = "A field validation error occurred, more details in userMessage",
        userMessage = userMessage.invoke()
    )
}
