package br.com.calculato.resourceutils

import io.ktor.client.request.HttpRequestBuilder
import io.ktor.http.HttpStatusCode
import io.ktor.util.StringValues
import org.bson.types.ObjectId
import java.time.LocalDate
import java.util.UUID

/**
 * Verifies the [T] Data Transfer Object's [name] property with the given [verification],
 * throwing a Bad Request error when does not
 */
inline fun <T> T.verifyParam(
    name: String,
    errorStatusCode: HttpStatusCode = HttpStatusCode.BadRequest,
    verification: (T) -> Boolean
) {
    if (!verification(this)) httpException(
        code = errorStatusCode,
        developerMessage = "$name parameter is illegal",
        userMessage = "O campo $name não foi informado corretamente."
    )
}

/**
 * Get and converts a [T] parameter type with given [name], @returns null when not present or invalid
 */
inline fun <reified T> StringValues.receiveOptionalParam(name: String): T? = getAll(name)?.firstOrNull().let {
    it.runCatching {
        when (T::class) {
            LocalDate::class -> LocalDate.parse(this) as T
            UUID::class -> UUID.fromString(this) as T
            ObjectId::class -> ObjectId(it) as T
            Boolean::class -> it?.toBoolean() as T
            Int::class -> it?.toInt() as T
            else -> it as T
        }
    }.getOrNull()
}

/**
 * Get and converts a [T] parameter type with given [name], throws HTTP exceptions when not present or not valid
 */
inline fun <reified T> StringValues.receiveParam(name: String): T = getAll(name)?.firstOrNull().let {
    if (it == null) httpException(developerMessage = "Required parameter $name is not present")
    it.runCatching {
        when (T::class) {
            LocalDate::class -> LocalDate.parse(this) as T
            UUID::class -> UUID.fromString(this) as T
            ObjectId::class -> ObjectId(it) as T
            Boolean::class -> it.toBoolean() as T
            Int::class -> it.toInt() as T
            else -> it as T
        }
    }
        .getOrElse { httpException(developerMessage = "Parameter $name does not contain a valid ${T::class.simpleName} value") }
}

/**
 * Get and convert String list to UUID list
 */
fun List<String>.toUUID(): List<UUID> {
    return this.map {
        try {
            UUID.fromString(it)
        } catch (e: IllegalArgumentException) {
            httpException(developerMessage = "$it is not a valid UUID")
        }
    }
}

/**
 * Get [this] and preparing the cursorRequest [T] to client request.
 */
inline fun <reified T> HttpRequestBuilder.buildCursorParam(cursor: T) = url.parameters.apply {
    when (T::class) {
        IdsCursorRequest::class -> {
            cursor as IdsCursorRequest
            cursor.cursor?.let { append(cursor::cursor.name, it) }
            append(cursor::size.name, cursor.size.toString())
            if (cursor.tags.isNotEmpty()) appendAll(cursor::tags.name, cursor.tags)
            append(cursor::orderProperty.name, cursor.orderProperty.name)
            append(cursor::order.name, cursor.order.name)
            if (cursor.ids.isNotEmpty()) appendAll(cursor::ids.name, cursor.ids.map { it.toString() })
            append(cursor::startDate.name, cursor.startDate.toString())
            append(cursor::endDate.name, cursor.endDate.toString())
        }
        CursorRequest::class -> {
            cursor as CursorRequest
            cursor.cursor?.let { append(cursor::cursor.name, it) }
            if (cursor.tags.isNotEmpty()) appendAll(cursor::tags.name, cursor.tags)
            append(cursor::size.name, cursor.size.toString())
            append(cursor::orderProperty.name, cursor.orderProperty.name)
            append(cursor::order.name, cursor.order.name)
            cursor.search?.let { append(cursor::search.name, it) }
        }
        else -> serverException(
            code = HttpStatusCode.InternalServerError,
            developerMessage = "${T::class.simpleName} is not an able CursorRequest"
        )
    }
}
