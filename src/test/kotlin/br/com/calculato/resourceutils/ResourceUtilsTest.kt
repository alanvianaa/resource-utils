package br.com.calculato.resourceutils


import com.google.gson.JsonPrimitive
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.ktor.util.InternalAPI
import io.ktor.util.StringValuesImpl
import org.bson.types.ObjectId
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.UUID

@InternalAPI
class ResourceUtilsTest : StringSpec({
    "getUuidParamTest" {
        val id = UUID.randomUUID()
        val stringValues = StringValuesImpl(
            values = mapOf(
                "id" to listOf(id.toString()),
                "notValidId" to listOf("anyString")
            )
        )

        val resultId = stringValues.receiveParam<UUID>("id")
        resultId shouldBe id

        stringValues.receiveParam<UUID>("id") shouldBe id

        stringValues.receiveOptionalParam<UUID>(name = "notPresent") shouldBe null
        stringValues.receiveOptionalParam<UUID>(name = "notValidId") shouldBe null

        val notPresentException = shouldThrow<HttpException> {
            stringValues.receiveParam<UUID>("notPresentId")
        }
        notPresentException.developerMessage shouldBe "Required parameter notPresentId is not present"

        val notValidException = shouldThrow<HttpException> {
            stringValues.receiveParam<UUID>("notValidId")
        }
        notValidException.developerMessage shouldBe "Parameter notValidId does not contain a valid UUID value"
    }

    "date parser" {
        val stringDate = "2020-02-04"
        val objectDate = LocalDate.of(2020, 2, 4)

        objectDate.toString() shouldBe stringDate
        LocalDate.parse(stringDate) shouldBe objectDate
    }

    "List<String> to List<UUID>"{
        with(listOf("e249d718-10eb-4cab-b75c-dfef3d118d23", "2d8ac682-3e33-47a3-9065-36f57c97089a").toUUID())
        {
            this[0] shouldBe UUID.fromString("e249d718-10eb-4cab-b75c-dfef3d118d23")
            this[1] shouldBe UUID.fromString("2d8ac682-3e33-47a3-9065-36f57c97089a")
        }

        shouldThrow<HttpException> {
            listOf("e249d718-10eb-4cab-b75c-dfef3d118d23", "<thisIsNotUuid>").toUUID()
        }.developerMessage shouldBe "<thisIsNotUuid> is not a valid UUID"
    }

    "getObjectIdParam"{
        val id = ObjectId("5edbe42e08284e2d921ce990")
        val stringValues = StringValuesImpl(
            values = mapOf(
                "id" to listOf(id.toString()),
                "notValidId" to listOf("anyString")
            )
        )
        val resultId = stringValues.receiveParam<ObjectId>("id")
        resultId shouldBe id

        val notPresentException = shouldThrow<HttpException> {
            stringValues.receiveParam<ObjectId>("notPresentId")
        }
        notPresentException.developerMessage shouldBe "Required parameter notPresentId is not present"

        val notValidException = shouldThrow<HttpException> {
            stringValues.receiveParam<ObjectId>("notValidId")
        }
        notValidException.developerMessage shouldBe "Parameter notValidId does not contain a valid ObjectId value"
    }

    "getDateParam"{
        val date = LocalDate.parse("2020-08-06")
        val stringValues = StringValuesImpl(
            values = mapOf(
                "date" to listOf(date.toString()),
                "notValidDate" to listOf("anyString")
            )
        )
        val resultId = stringValues.receiveParam<LocalDate>("date")
        resultId shouldBe date

        val notPresentException = shouldThrow<HttpException> {
            stringValues.receiveParam<LocalDate>("notPresentDate")
        }
        notPresentException.developerMessage shouldBe "Required parameter notPresentDate is not present"

        val notValidException = shouldThrow<HttpException> {
            stringValues.receiveParam<LocalDate>("notValidDate")
        }
        notValidException.developerMessage shouldBe "Parameter notValidDate does not contain a valid LocalDate value"
    }

    "getBooleanParam"{
        val bool = "true"
        val stringValues = StringValuesImpl(
            values = mapOf(
                "indPago" to listOf(bool),
                "notBoolean" to listOf("anyString")
            )
        )
        stringValues.receiveParam<Boolean>("indPago") shouldBe true

        val notPresentException = shouldThrow<HttpException> {
            stringValues.receiveParam<Boolean>("notPresent")
        }
        notPresentException.developerMessage shouldBe "Required parameter notPresent is not present"

        stringValues.receiveParam<Boolean>("notBoolean") shouldBe false

        stringValues.receiveOptionalParam<Boolean>("indPago") shouldBe true

        stringValues.receiveOptionalParam<Boolean>("notBoolean") shouldBe false
    }
    "getIntParam"{
        val int = "202010"
        val stringValues = StringValuesImpl(
            values = mapOf(
                "periodo" to listOf(int),
                "notInt" to listOf("anyString")
            )
        )

        stringValues.receiveParam<Int>("periodo") shouldBe 202010

        val notPresentException = shouldThrow<HttpException> {
            stringValues.receiveParam<Int>("notPresent")
        }
        notPresentException.developerMessage shouldBe "Required parameter notPresent is not present"

        val notValidException = shouldThrow<HttpException> {
            stringValues.receiveParam<Int>("notInt")
        }
        notValidException.developerMessage shouldBe "Parameter notInt does not contain a valid Int value"

        stringValues.receiveOptionalParam<Int>("periodo") shouldBe 202010
    }
    "DurationSerializer"{
        DurationSerializer.deserialize(
            json = JsonPrimitive("14:15:30"),
            typeOfT = null,
            context = null
        ) shouldBe Duration.ofHours(14).plusMinutes(15).plusSeconds(30)
        DurationSerializer.deserialize(
            json = JsonPrimitive("27:15"),
            typeOfT = null,
            context = null
        ) shouldBe Duration.ofHours(27).plusMinutes(15)

        shouldThrow<HttpException> {
            DurationSerializer.deserialize(json = JsonPrimitive("12"), typeOfT = null, context = null)
        }.developerMessage shouldBe "Not a valid 'HH:mm:ss' or 'HH:mm' duration format"
    }
    "ObjectIdSerializer"{
        ObjectIdSerializer.deserialize(
            json = JsonPrimitive("5fd77c2c16451f0ab2e809b9"),
            typeOfT = null,
            context = null
        ) shouldBe ObjectId("5fd77c2c16451f0ab2e809b9")

        shouldThrow<HttpException> {
            ObjectIdSerializer.deserialize(
                json = JsonPrimitive("notObjectId"),
                typeOfT = null,
                context = null
            )
        }.developerMessage shouldBe "Not a valid ObjectId"
    }
    "TimeSerializer"{
        TimeSerializer.deserialize(
            json = JsonPrimitive("14:35"),
            typeOfT = null,
            context = null
        ) shouldBe LocalTime.of(14, 35)

        shouldThrow<HttpException> {
            TimeSerializer.deserialize(
                json = JsonPrimitive("25:35"),
                typeOfT = null,
                context = null
            )
        }.developerMessage shouldBe "Not a valid 'HH:mm' time format"
    }
    "DateSerializer"{
        DateSerializer.deserialize(
            json = JsonPrimitive("2020-10-01"),
            typeOfT = null,
            context = null
        ) shouldBe LocalDate.of(2020, 10, 1)

        shouldThrow<HttpException> {
            DateSerializer.deserialize(
                json = JsonPrimitive("2020-10-1"),
                typeOfT = null,
                context = null
            )
        }.developerMessage shouldBe "Not a valid 'yyyy-MM-dd' date format"
    }
    "DateTimeSerializer"{
        DateTimeSerializer.deserialize(
            json = JsonPrimitive("2020-10-01T15:30:25"),
            typeOfT = null,
            context = null
        ) shouldBe LocalDateTime.of(2020, 10, 1, 15, 30, 25)

        DateTimeSerializer.deserialize(
            json = JsonPrimitive("2020-10-01T15:30"),
            typeOfT = null,
            context = null
        ) shouldBe LocalDateTime.of(2020, 10, 1, 15, 30)

        shouldThrow<HttpException> {
            DateTimeSerializer.deserialize(
                json = JsonPrimitive("2020-10-01T15"),
                typeOfT = null,
                context = null
            )
        }.developerMessage shouldBe "Not a valid 'yyyy-MM-ddTHH:mm:ss' date time format"

        shouldThrow<HttpException> {
            DateTimeSerializer.deserialize(
                json = JsonPrimitive("2020-10-1"),
                typeOfT = null,
                context = null
            )
        }.developerMessage shouldBe "Not a valid 'yyyy-MM-ddTHH:mm:ss' date time format"
    }
})
